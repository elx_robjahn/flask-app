# our base image
FROM centos:latest

# expect to have the build number passed in.  Build number is in the name of tar file.
ARG APP_DIST
ENV APP_DIST ${APP_DIST}

# need to expose all port so that can mapped outside of the container
EXPOSE 5000

# install the pip and its pre-requiste
RUN yum -y install epel-release; yum clean all
RUN yum -y install python-pip; yum clean all

# install Python modules needed by the Python app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# copy in the app tar file and uncompress it
COPY artifacts/${APP_DIST}.tar.gz /tmp/${APP_DIST}.tar.gz
RUN tar -xvf /tmp/${APP_DIST}.tar.gz -C /tmp/

# run the application
CMD python /tmp/${APP_DIST}/src/flask-restful-app.py /tmp/${APP_DIST}/src/flask-restful-app.db


