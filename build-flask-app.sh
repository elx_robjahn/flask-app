#!/bin/bash

# remove old distribution files
rm -rf dist/*

# build the new build artifact
python setup.py bdist_rpm

# copy artifact file out
cp dist/*.tar.gz artifacts/
