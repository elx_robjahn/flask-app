#!/bin/bash

# these just clean up any running or unused containers
docker container prune -f
docker container stop flask-restful-app
docker container rm flask-restful-app

# start up the container which will start the app
clear 
echo ------------------------------------------------------- 
echo Flask app accessable via: http://localhost:8080/gettask
echo ------------------------------------------------------- 
# default flask port is 5000, but map to more standard one
docker run -p 8080:5000 --name flask-restful-app --rm flask-restful-app
