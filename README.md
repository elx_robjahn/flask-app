Flask Demo App
==============
Web app built using Flask module.  
Made of single python script and file based SQL object file
4 services to modify a “task” entity (get, update, delete, add)

Usage
-----
This project has a set of shell scripts from command line in the root folder to build the app and run it.
You need to add an "artifacts" directory to the root folder.  This folder is in .gitignore

* "build-flask-app.sh" script will call python setup.py and make tar.gz file with date-time in the name.  It will copy the tar.gz file in to the artifacts directory
* "build-docker-image.sh" script takes as an argument the tar.gz file name with out the extention. This will make a docker image called "flask-restful-app" and copy in the flask app version passed in
* "run-flask-app.sh" script will run the latest "flask-restful-app" docker image which will start the demo app. The demo app is accessable on http://localhost:8080
* "Flaskapp.postman_collection" file can be imported into Chrome postman app to test the application services 
* "setup.py" python scripts used to make the distribution files.
* "requirements.txt" used during the Docker build process as an argument to pip install for the python app dependencies

