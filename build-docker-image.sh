#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Missing flask app version argument, expect value like flask-app-03117-174448"
  exit 1
fi

file=artifacts/"$1".tar.gz
if [ ! -f "$file" ]
then
  echo "File '${file}' not found."
  exit 1
fi

echo Building with $1...
# build the docler image
docker build -t flask-restful-app --build-arg APP_DIST=$1 .
