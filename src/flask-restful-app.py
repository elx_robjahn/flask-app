#!/usr/bin/env python

"""
synopsis:
    Run flask-rest app with SQLObject database.
    Implements REST CRUD access to a set of tasks (name, description).
usage:
    python flask-restful-app.py <db-file-name>
example:
    python flask-restful-app.py flask-restful-app.db
"""

# Reference: http://davekuhlman.org/python-flask-restful-rest-crud.html
# API reference: https://github.com/adnan-kamili/swagger-response-template

# We try to enable our app to run under both Python 2 and Python 3.
from __future__ import print_function
import sys
import os
from flask import Flask, request
from flask_restful import abort, Api, Resource
import sqlobject

app = Flask(__name__)
api = Api(app)

#
# The following can be used for configuration.
##app.config.update(
##    DEBUG=True,
##    #SERVER_NAME='localhost:8080',
##    #SERVER_NAME='localhost:5000',
##    SERVER_NAME='localhost:8001',
##)

# Define the host and post.
#SERVER_HOST = 'localhost'

# per flask docs - Set this to '0.0.0.0' to have the server available externally as well. Defaults to '127.0.0.1'.
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 5000


#
# Define the objects to be stored in the SQLObject database.
class TaskDef(sqlobject.SQLObject):
    task_name = sqlobject.StringCol()
    description = sqlobject.StringCol()


def open_db(db_filename):
    """Open the database.  If it does not exist, create it."""
    create = False
    db_filename = os.path.abspath(db_filename)
    if not os.path.exists(db_filename):
        create = True
    connection_string = 'sqlite:' + db_filename
    connection = sqlobject.connectionForURI(connection_string)
    sqlobject.sqlhub.processConnection = connection
    if create:
        TaskDef.createTable()


def get_task_by_name(task_name):
    """Retrieve a task from the DB.  Abort if it does not exist."""
    results = TaskDef.select(TaskDef.q.task_name == task_name)
    if results.count() < 1:
        abort(404, message="Task {} doesn't exist".format(task_name))
    return results[0]


class TaskList(Resource):
    """Handle operations on the list of tasks.

    Enable a GET command to get a list of tasks or a single task.
    Enable a POST command to create and add a new task.
    Enable a PUT command to update a task.
    """
    def __init__(self, *args, **kwargs):
        super(TaskList, self).__init__(*args, **kwargs)

    def get(self, task_name=None):
        """Return a collection of existing tasks or a single task."""
        if task_name is None:
            collection = {}
            results = TaskDef.select()
            for task in results:
                collection[task.task_name] = task.description
            return { 'tasks' : collection }
        else:
            task = get_task_by_name(task_name)
            content = {
                'task_name': task_name,
                'description': task.description,
            }
            return content

    def delete(self, task_name):
        """Delete a single task."""
        task = get_task_by_name(task_name)
        TaskDef.delete(task.id)
        content = {
            'message': 'Deleted Task {} successfully'.format(task_name)
        }
        #return "<p>Deleted task: {}</p>".format(task_name)
        return content

    def post(self):
        """Create and save a new task."""
        task_name = request.form['task_name']
        description = request.form['description']
        TaskDef(
            task_name=task_name,
            description=description)
        task_json = {
            'task_name': task_name,
            'description': description}
        content = {
            'message': 'Task {} created successfully'.format(task_name)
        }
        return content

    def put(self):
        """Create or Update task."""
        task_name = request.form['task_name']
        description = request.form['description']
        task = get_task_by_name(task_name)
        task.description = description
        task_json = {
            'task_name': task_name,
            'description': description}
        content = {
            'message': 'Task {} updated successfully'.format(task_name)
        }
        return content


#
# Set up the api resource routing.
#
api.add_resource(
    TaskList,
    # Get a list of tasks.
    '/gettask',
    # Get one task by name.
    '/gettask/<task_name>',
    # Add a task.  Form data: task_name, description.
    '/addtask',
    # Update a task.  Form data: task_name, description.
    '/updatetask',
    # Delete one task.`
    '/delete/<task_name>',
)


def main():
    #import pdb; pdb.set_trace()
    args = sys.argv[1:]
    if len(args) != 1:
        sys.exit(__doc__)
    db_filename = args[0]
    open_db(db_filename)
    app.run(debug=True, host=SERVER_HOST, port=SERVER_PORT)


if __name__ == '__main__':
    main()
