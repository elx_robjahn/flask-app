#!/usr/bin/env python

from distutils.core import setup
from time import strftime, localtime

version = ''

def get_version():
    # return a time stamp as the version number
    global version 
    version = strftime('%m%d%y-%H%M%S',localtime())
    return version

setup(name='flask-app',
      version=get_version(),
      description='REST services demo application',
      author='Rob Jahn',
      author_email='rob.jahn@elyxor.com',
      url='http://www.elyxor.com',
      scripts=['src/flask-restful-app.py'],
      data_files=[('', ['src/flask-restful-app.db'])]
     )

print 'Built version: ' + version
